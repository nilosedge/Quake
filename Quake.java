// Fig. 14.5: DeitelLoop4.java
// Load an array of images, loop through the array,
// and display each image.
import java.applet.Applet;
import java.awt.*;

public class Quake extends Applet implements Runnable {
   private Image back,cool;
   private int x=0,y=0,xmovement=1,ymovement=1,height=100,width=400,sleepTime =5,randomvalue=2; 

   // The next two objects are for double-buffering
   private Graphics gContext; // off-screen graphics context 
   private Image buffer;      // buffer in which to draw image


   private Thread animate;      // animation thread

   // load the images when the applet begins executing
   public void init()
   {
      buffer = createImage( width,height ); // create image buffer
      gContext = buffer.getGraphics(); // get graphics context
      // set background of buffer to white
      gContext.setColor( Color.black );
      gContext.fillRect( 0, 0, width, height);

	cool = getImage( getDocumentBase(),
"cool.gif" );
         back = getImage( getDocumentBase(),
            "bk.jpg" );


   }                                     

   // start the applet
   public void start()
   {
      // always start with 1st image
 gContext.setColor( Color.black );
      gContext.fillRect( 0,0,width,height);

      // create a new animation thread when user visits page
      if ( animate == null ) {
         animate = new Thread( this );
         animate.start();
      }
   }

   // terminate animation thread when user leaves page
   public void stop()
   {
      if ( animate != null ) {
         animate.stop();
         animate = null;
      }
   }

   // display the image in the Applet's Graphics context
   public void paint( Graphics g )
   {
if(x<0){xmovement=((int)(Math.random()*randomvalue)+1);}
if(x>(width-72)){
xmovement=((int)(Math.random()*randomvalue)+1);
xmovement=(xmovement * (-1));}
if(y<0){ymovement=((int)(Math.random()*randomvalue)+1);}
if(y>(height-80)){
ymovement=((int)(Math.random()*randomvalue)+1);
ymovement=(ymovement * (-1));}

x=x+xmovement;
y=y+ymovement;
      g.drawImage( buffer, 0, 0, this );
   }

   // override update to eliminate flicker
   public void update( Graphics g ) { paint( g ); }

   public void run()
   {
      while ( true ) {
            // clear previous image from buffer
gContext.fillRect( 0,0,width,height);
 
            // draw new image in buffer
            gContext.drawImage(
               cool, x, y, this );
   

         try {
            Thread.sleep( sleepTime );
         }
         catch ( InterruptedException e ) {
            showStatus( e.toString() );
         }

         repaint();  // display buffered image
      }
   }
}
                                        
